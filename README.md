Use Alexa Retro Skill to manage your Jira board. You can:

- getStoryStatus.
- deleteStory.
- assignUserToStory.
- transitStory.
- createStory.
- getStoryPoint.
- estimateStory.

This Skill use Oauth authentication service and the REST api is tested under the tests folder.

If you want to use Basic Auth, you will need to change the code where you see the URL property.