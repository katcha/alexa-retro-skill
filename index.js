'use strict';

//Use alexa-app node package.
var alexa = require('alexa-app');

//HTTP status code readable.
var httpStatusCode = require('http-status-codes');

//Module dedicated to the issues API call.
var story = require('./modules/atlassian-jira-rest-api-for-issues.js');

//Create a new instance of alexa-app.
var app = new alexa.app();

/**
* This will be triggered at the launch of the skill.
* @param request
* @param response
**/
app.launch(function(request,response) {
  response.say("Welcome to Alexa Retro. You can manage your Agile team with this skill. If you need any help, just say Help. To leave say bye.");

  var accessToken = request.sessionDetails.accessToken;

  if (accessToken === null) {
    response.linkAccount().shouldEndSession(false).say('Your Jira account is not linked to me. Please use the Alexa App to allow your Jira account.');
    return true;
  } else {
    console.log('has a token==== ', accessToken);
  }
});

/**
* HelpIntent: Explain how the user can interact with alexa-retro-skill.
**/
app.intent('HelpIntent', {
  "slots": {},
  "utterances": [
    "help"
  ]
}, function (request, response) {
  response.say("You can say something like Alexa, ask Retro what is the status of story 123");
  console.error("ACCESS TOKEN HERE=====", request.sessionDetails.accessToken);
});

/**
* GetStoryStatus: Alexa will tell to the user the current status of a story, for example "To Do, in progress, done ..."
*
* One-fire dialog example:
*   - User: Alexa, ask retro to get the status of story 5643
*   - Alexa: The status of story 5643 is in progress.
**/
app.intent('GetStoryStatus', {
  "slots": {"StoryTicketNumber": "AMAZON.NUMBER"},
  "utterances": [

    "ask retro to tell me status of story {StoryTicketNumber}",
    "tell retro to get the status of story {StoryTicketNumber}",
    "get status of story {StoryTicketNumber}",
    "tell me the status of story {StoryTicketNumber}"
  ]
},
function (request, response) {
  var storyTicketNumber = request.slot("StoryTicketNumber");
  var reprompt = 'Tell me a correct story ticket number';

  if ( typeof storyTicketNumber === 'undefined' || storyTicketNumber === '' || storyTicketNumber === null ) {
    var prompt = 'I didn\'t hear a story ticket number.';
    response.say(prompt).reprompt(reprompt).shouldEndSession(false);
    return true;
  } else {
    story.getStoryStatus(storyTicketNumber)
    .then((res) => {
      response.shouldEndSession(false).say('The status of story ' + storyTicketNumber + ' is ' + res).send();
    }).catch((err) => {
      if (err === httpStatusCode.UNAUTHORIZED) {
        response.linkAccount().shouldEndSession(false).say('You are not allowed to do this request. Please accept the permissions for Jira application, in your Alexa dashboard.');
      } else if (err === httpStatusCode.NOT_FOUND) {
        response.shouldEndSession(true).say("I'm sorry but I am unable to find the status of story " + storyTicketNumber).send();
      } else {
        response.shouldEndSession(true).say("I'm sorry, the request failed for some reason. Please try later on.").send();
      }
    });
  }
  return false;
});

/**
* assignUserToStory: Alexa will assign a story to the given username.
* One-fire dialog example:
*  - User: Alexa, ask retro to assign story 6574 to John Doe
*  - Alexa: Story 6574 is now assigned to John Doe
**/
app.intent('AssignUser', {
  "slots": {
    "StoryTicketNumber": "AMAZON.NUMBER",
    "Username": "AMAZON.LITERAL"
  },
  "utterances": [
    "ask retro to assign story {StoryTicketNumber} to {Username}",
    "tell retro to assign story {StoryTicketNumber} to {Username}",
    "assign story {StoryTicketNumber} to {Username}"
  ]
},
function(request, response) {
  var storyTicketNumber = request.slot('StoryTicketNumber');
  var username = request.slot('Username');
  var reprompt = 'Tell me an issue number';

  if ( typeof storyTicketNumber === 'undefined' || storyTicketNumber === '' || storyTicketNumber === null) {
    var prompt = 'I didn\'t hear an issue ticket number. Tell me an issue number.';
    response.say(prompt).reprompt(reprompt).shouldEndSession(false);
    return true;
  } else if (typeof username === 'undefined' || username === '' || username === null) {
    var prompt = 'I didn\'t hear a username to assign. Tell me a correct username.';
    response.say(prompt).reprompt(reprompt).shouldEndSession(false);
    return true;
  } else {
    story.assignUserToStory(username, storyTicketNumber)
    .then((res) => {
      response.shouldEndSession(false).say('The story ' + storyTicketNumber + ' is now assigned to ' + username).send();
    }).catch((err) => {
      if (err === httpStatusCode.UNAUTHORIZED) {
        response.linkAccount().shouldEndSession(false).say('You are not allowed to do this request. Please accept the permissions for Jira application, in your Alexa dashboard.');
      } else if (err === httpStatusCode.NOT_FOUND) {
        response.shouldEndSession(true).say("I'm sorry but I am unable to find the story or the given username").send();
      } else {
        response.shouldEndSession(true).say("I'm sorry, the request failed for some reason. Please try later on.").send();
      }

    });
  }
  return false;
});

/**
* Delete a story from the board.
* One-fire dialog example:
*  - User: Alexa, ask retro to delete story 6574
*  - Alexa: Story 6574 is now deleted from your board.
**/
app.intent('DeleteStory', {
  "slots": {
    "StoryTicketNumber": "AMAZON.NUMBER"
  },
  "utterances": [
    "ask retro to delete story {StoryTicketNumber}",
    "tell retro to delete story {StoryTicketNumber}",
    "delete story {StoryTicketNumber}",
    "remove story {StoryTicketNumber}"
  ]
},
function(request, response) {
  var storyTicketNumber = request.slot('StoryTicketNumber');
  var reprompt = 'Tell me an issue number';

  if ( typeof storyTicketNumber === 'undefined' || storyTicketNumber === '' || storyTicketNumber === null) {
    var prompt = 'I didn\'t hear an issue ticket number. Tell me an issue number.';
    response.say(prompt).reprompt(reprompt).shouldEndSession(false);
    return true;
  } else {
    story.deleteStory(storyTicketNumber)
    .then((res) => {
      response.shouldEndSession(false).say('The story ' + storyTicketNumber + ' is now deleted from your board.').send();
    }).catch((err) => {
      if (err === httpStatusCode.UNAUTHORIZED) {
        response.linkAccount().shouldEndSession(false).say('You are not allowed to do this request. Please accept the permissions for Jira application, in your Alexa dashboard.');
      } else if (err === httpStatusCode.NOT_FOUND) {
        response.shouldEndSession(true).say("I'm sorry but I am unable to delete the story " + storyTicketNumber).send();
      } else {
        response.shouldEndSession(true).say("I'm sorry, the request failed for some reason. Please try later on.").send();
      }

    });
  }
  return false;
});

/**
* Transit the status of a story.
* One-fire dialog example:
*  - User: Alexa, ask retro to move story 6574 to in progress.
*  - Alexa: Story 6574 is now in progress.
**/
app.intent('TransitStory', {
  "slots": {
    "StoryTicketNumber": "AMAZON.NUMBER",
    "Column": "AMAZON.LITERAL"
  },
  "utterances": [
    "ask retro to move story {StoryTicketNumber} to {Column}",
    "tell retro to move story {StoryTicketNumber} to {Column}",
    "move story {StoryTicketNumber} to {Column}",
    "put story {StoryTicketNumber} in {Column}"
  ]
},
function(request, response) {
  var storyTicketNumber = request.slot('StoryTicketNumber');
  var column = request.slot('Column');
  var reprompt = 'Tell me an issue number';

  if ( typeof storyTicketNumber === 'undefined' || storyTicketNumber === '' || storyTicketNumber === null) {
    var prompt = 'I didn\'t hear an issue ticket number. Tell me an issue number.';
    response.say(prompt).reprompt(reprompt).shouldEndSession(false);
    return true;
  } else if ( typeof column === 'undefined' || column === '' || column === null) {
    var prompt = 'I didn\'t hear a column name. Tell me a column name, like ready for development.';
    response.say(prompt).shouldEndSession(false);
    return true;
  } else {
    story.transitStory(storyTicketNumber, column)
    .then((res) => {
      response.shouldEndSession(false).say('The story ' + storyTicketNumber + ' is now in ' + column + ' column.').send();
    }).catch((err) => {
      if (err === httpStatusCode.UNAUTHORIZED) {
        response.linkAccount().shouldEndSession(false).say('You are not allowed to do this request. Please accept the permissions for Jira application, in your Alexa dashboard.');
      } else if (err === httpStatusCode.NOT_FOUND) {
        response.shouldEndSession(true).say("I'm sorry but I am unable to do this transition for story " + storyTicketNumber).send();
      } else {
        response.shouldEndSession(true).say("I'm sorry, the request failed for some reason. Please try later on.").send();
      }
    });
  }
  return false;
});

/**
* Get story estimation point.
**/
app.intent('GetStoryPoint', {
  "slots": {
    "StoryTicketNumber": "AMAZON.NUMBER",
  },
  "utterances": [
    "get the story point for {StoryTicketNumber}",
    "what is the estimation for {StoryTicketNumber}",
    "get estimation for {StoryTicketNumber}"
  ]
},
function(request, response) {
  var storyTicketNumber = request.slot('StoryTicketNumber');
  var reprompt = 'Tell me an issue number';

  if ( typeof storyTicketNumber === 'undefined' || storyTicketNumber === '' || storyTicketNumber === null) {
    var prompt = 'I didn\'t hear an issue ticket number. Tell me an issue number.';
    response.say(prompt).reprompt(reprompt).shouldEndSession(false);
    return true;
  } else {
    story.getStoryPoint(storyTicketNumber)
    .then((res) => {
      response.shouldEndSession(false).say('The story ' + storyTicketNumber + ' have ' + res + ' points').send();
    }).catch((err) => {
      if (err === httpStatusCode.UNAUTHORIZED) {
        response.linkAccount().shouldEndSession(false).say('You are not allowed to do this request. Please accept the permissions for Jira application, in your Alexa dashboard.');
      } else if (err === httpStatusCode.NOT_FOUND) {
        response.shouldEndSession(true).say("I'm sorry but I am unable to find story " + storyTicketNumber).send();
      } else {
        response.shouldEndSession(true).say("I'm sorry, the request failed for some reason. Please try later on.").send();
      }
    });
  }
  return false;
});

/**
* Estimate a story
**/
app.intent('EstimateStory', {
  "slots": {
    "StoryTicketNumber": "AMAZON.NUMBER",
    "StoryPoint": "AMAZON.NUMBER",

  },
  "utterances": [
    "estimate story {StoryTicketNumber} to {StoryPoint}",
    "put {StoryPoint} to story {StoryTicketNumber}"
  ]
},
function(request, response) {
  var storyTicketNumber = request.slot('StoryTicketNumber');
  var reprompt = 'Tell me an issue number';

  var storyPoint = request.slot('StoryPoint');

  if ( typeof storyTicketNumber === 'undefined' || storyTicketNumber === '' || storyTicketNumber === null) {
    var prompt = 'I didn\'t hear an issue ticket number. Tell me an issue number.';
    response.say(prompt).reprompt(reprompt).shouldEndSession(false);
    return true;
  } else if (typeof storyPoint === 'undefined' || storyPoint === '' || storyPoint === null) {
    var prompt = 'I didn\'t hear a story point. Tell me an estimation.';
    response.say(prompt).shouldEndSession(false);
    return true;
  } else {
    story.estimateStory(storyTicketNumber, storyPoint)
    .then((res) => {
      response.shouldEndSession(false).say('The story ' + storyTicketNumber + ' is now estimated with ' + storyPoint + ' points').send();
    }).catch((err) => {
      if (err === httpStatusCode.UNAUTHORIZED) {
        response.linkAccount().shouldEndSession(false).say('You are not allowed to do this request. Please accept the permissions for Jira application, in your Alexa dashboard.');
      } else if (err === httpStatusCode.NOT_FOUND) {
        response.shouldEndSession(true).say("I'm sorry but I am unable to find story " + storyTicketNumber).send();
      } else {
        response.shouldEndSession(true).say("I'm sorry, the request failed for some reason. Please try later on.").send();
      }
    });
  }
  return false;
});

/**
* Ask Alexa to start the Estimation story counter.
**/
app.intent('EstimationCounter', {
  "utterances": [
    "start the counter",
    "start to count",
    "ask retro to count"
  ]
},
function(request, response) {
  response.say("One. Two. Three.").send();
});

/**
* Ask Alexa to give her estimation about a story.
* This is the funny part of alexa-retro-skill! :D
**/
app.intent('RandomStoryEstimationIntent', {
  "utterances": [
    "alexa estimate story",
    "how much do you think this story is",
    "how many point is this story"
  ]
},
function(request, response) {
  var agileStoryPoint = ['one', 'three', 'five', 'eight'];
  var rand = agileStoryPoint[Math.floor(Math.random() * agileStoryPoint.length)];

  response.say('I think this story is definitely a ' + rand + ' story point!').send();
});

/**
* Create a new Story on the board.
**/

//Add AWS Lambda function.
exports.handler = app.lambda();

/**
* This is used to generate the schema and utterances output.
**/
if ((process.argv.length === 3) && (process.argv[2] === 'schema'))
{
  console.log (app.schema(), "\n");
  console.log (app.utterances());
}
