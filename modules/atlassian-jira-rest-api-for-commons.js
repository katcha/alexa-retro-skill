'use strict';

var https          = require('https');
var httpStatusCode = require('http-status-codes');

/**
* Will return the available project name of the Jira interface.
* This will also be used to generate the story key name.
* GET /rest/api/2/project
**/
function getAllProjects(projectName) {

  return new Promise((resolve, reject) => {
    const request = https.get('https://admin:Encarta2016@devniz.atlassian.net/rest/api/latest/project', (response) => {
      if (response.statusCode < 200 || response.statusCode > 299) {
        reject(response.statusCode);
      }

      const body = [];

      response.on('data', (chunk) => {
        body.push(chunk);
      });

      response.on('end', () => {
        try {
          var res = JSON.parse(body.join(''));
          //Look for the key of the given project name.
          for(var key in res) {
            if ( res[key].name.toLowerCase() === projectName.toLowerCase() ) {
              if (typeof res[key].key === 'undefined' || res[key].key === null || res[key].key === '') {
                reject(httpStatusCode.NOT_FOUND);
              } else {
                resolve(res[key].key);
              }
            }

            //We can now return the project key associated to the given project name.
            resolve(projectKey);
          }
        } catch(e) {
          reject(httpStatusCode.NOT_FOUND);
        }
      });

    });

    request.on('error', (err) => reject(err))
    request.end();
  })

}

/**
* Return the story label constructed from the fetched
* project name;
* @param String storyTicketNumber
* @return String storyLabel
**/
function constructStoryLabel(storyTicketNumber) {
  var projectKey = 'AL';

  return projectKey + '-' + storyTicketNumber;
}

var apiCommon = {
  constructStoryLabel: constructStoryLabel
};

module.exports = apiCommon;
