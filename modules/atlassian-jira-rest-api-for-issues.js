'use strict';

var https          = require('https');
var req            = require('request');
var common         = require('./atlassian-jira-rest-api-for-commons');
var httpStatusCode = require('http-status-codes');

/**
* Get the status of an issue based on the given issue id or key.
* /GET /rest/api/2/issue/{issueIdOrKey}
* @param String storyTicketNumber
**/
function getStoryStatus(storyTicketNumber) {

  var constructedStoryTicketNumber = common.constructStoryLabel(storyTicketNumber);

  return new Promise((resolve, reject) => {
    // select http or https module, depending on reqested url
    const request = https.get(
      'https://admin:Password123@devniz.atlassian.net/rest/api/latest/issue/' +
      constructedStoryTicketNumber,
      (response) => {
        // handle http errors
        if (response.statusCode < 200 || response.statusCode > 299) {
          reject(response.statusCode);
        }

        // temporary data holder
        const body = [];

        // on every content chunk, push it to the data array
        response.on('data', (chunk) => {
          body.push(chunk);
        });

        // we are done, resolve promise with those joined chunks
        response.on('end', () => {
          try {
            var result = JSON.parse(body.join(''));
            resolve(result.fields.status.name);
          } catch(e) {
            reject(httpStatusCode.NOT_FOUND);
          }
        });

      });

      // handle connection errors of the request
      request.on('error', (err) => reject(err))
      request.end();
    })

  }

  /**
  * Delete an existant issue
  * DELETE /rest/api/2/issue/{issueIdOrKey}
  * @param String storyTicketNumber
  **/
  function deleteStory(storyTicketNumber) {
    var constructedStoryTicketNumber = common.constructStoryLabel(storyTicketNumber);

    return new Promise((resolve, reject) => {
      const request = req.delete('https://admin:Password123@devniz.atlassian.net/rest/api/latest/issue/' + constructedStoryTicketNumber, (error, response, body) => {

        if (response.statusCode < 200 || response.statusCode > 299) {
          reject(response.statusCode);
        } else {
          resolve(response.statusCode);
        }
      });

      // handle connection errors of the request
      request.on('error', (err) => reject(err))
      request.end();
    });

  }

  /**
  * Assign an existant User to a story.
  * PUT /rest/api/2/issue/{issueIdOrKey}/assignee
  * @param String storyTicketNumber
  * @param String Username
  **/
  function assignUserToStory(username, storyTicketNumber) {
    var constructedStoryTicketNumber = common.constructStoryLabel(storyTicketNumber);

    return new Promise((resolve, reject) => {
      const request = req({
        url: 'https://admin:Password123@devniz.atlassian.net/rest/api/latest/issue/' + constructedStoryTicketNumber,
        method: 'PUT',
        json: {
          "fields": {
            "assignee":{ "name": username }
          }
        }
      },
      (error, response, body) => {
        if (response.statusCode < 200 || response.statusCode > 299) {
          reject(response.statusCode);
        } else {
          resolve(response.statusCode);
        }
      });
    });

  }

  /**
  * Get the list of possible transition.
  * GET /rest/api/2/issue/{issueIdOrKey}/transitions?expand=transitions.fields
  * @return array TransitionIds
  **/
  function getListOfTransition(storyTicketNumber) {

    return new Promise((resolve, reject) => {
      const request = https.get(
        'https://admin:Password123@devniz.atlassian.net/rest/api/latest/issue/' +
        storyTicketNumber +
        '/transitions?expand=transitions.fields',
        (response) => {
          if (response.statusCode < 200 || response.statusCode > 299) {
            reject(response.statusCode);
          }

          const body = [];

          response.on('data', (chunk) => {
            body.push(chunk);
          });

          response.on('end', () => {
            try {
              var result = JSON.parse(body.join(''));
              resolve(result['transitions']);
            } catch(e) {
              reject(httpStatusCode.NOT_FOUND);
            }
          });

        });

        request.on('error', (err) => reject(err))
        request.end();
      })

    }

    /**
    * Do a transition of a story.
    * POST /rest/api/2/issue/{issueIdOrKey}/transitions
    * @param String storyTicketNumber
    **/
    function transitStory(storyTicketNumber, column) {
      var constructedStoryTicketNumber = common.constructStoryLabel(storyTicketNumber);

      //We need to get a list of possible transition first.
      return getListOfTransition(constructedStoryTicketNumber).then((res) => {
        for(var key in res) {
          //Look for the requested column in the transitions fields object.
          if ( res[key].name.toLowerCase() === column.toLowerCase() ) {
            var transitId = res[key].id;
          }
        }

        //We can now do the issue transit in this nested Promise.
        if (typeof transitId === 'undefined' || transitId === null || transitId === '') {
          return httpStatusCode.NOT_FOUND;
        } else {
          return new Promise((resolve, reject) => {
            const request = req({
              url: 'https://admin:Password123@devniz.atlassian.net/rest/api/latest/issue/' + constructedStoryTicketNumber + '/transitions',
              method: 'POST',
              json: {
                "transition": {
                  "id": transitId
                }
              }
            },
            (error, response, body) => {
              if (response.statusCode < 200 || response.statusCode > 299) {
                reject(response.statusCode);
              } else {
                resolve(response.statusCode);
              }
            });
          });
        }
      })

    };

    /**
    * Create a new story in the board.
    * POST /rest/api/2/issue
    * @param String summary
    * @param String description
    * @param String issuetype
    **/
    function createStory(summary, description, issuetype) {

      return new Promise((resolve, reject) => {
        const request = req({
          url: 'https://admin:Password123@devniz.atlassian.net/rest/api/latest/issue/',
          method: 'POST',
          json: {
            "fields": {
              "project": {
                "key": "AL"
              },
              "summary": summary,
              "description": description,
              "issuetype": {
                "name": issuetype
              }
            }
          }
        },
        (error, response, body) => {
          if (response.statusCode < 200 || response.statusCode > 299) {
            reject(response.statusCode);
          } else {
            resolve(response.statusCode);
          }
        });
      });

    }

    /**
    * Get current story points.
    **/
    function getStoryPoint(storyTicketNumber) {

      var constructedStoryTicketNumber = common.constructStoryLabel(storyTicketNumber);

      return new Promise((resolve, reject) => {
        const request = https.get(
          'https://admin:Password123@devniz.atlassian.net/rest/api/latest/issue/' +
          constructedStoryTicketNumber,
          (response) => {
            if (response.statusCode < 200 || response.statusCode > 299) {
              reject(response.statusCode);
            }

            const body = [];

            response.on('data', (chunk) => {
              body.push(chunk);
            });

            response.on('end', () => {
              try {
                var result = JSON.parse(body.join(''));
                resolve(result.fields.customfield_10129);
              } catch(e) {
                reject(httpStatusCode.NOT_FOUND);
              }
            });

          });

          request.on('error', (err) => reject(err))
          request.end();
        })

      }

      /**
      * Estimate a story, Alexa will start a counter before the Team estimation.
      * PUT /rest/api/2/issue/{issueIdOrKey}
      * @param String storyTicketNumber
      * @param Integer storyPoint
      **/
      function estimateStory(storyTicketNumber, storyPoint) {
        var constructedStoryTicketNumber = common.constructStoryLabel(storyTicketNumber);

        return new Promise((resolve, reject) => {
          const request = req({
            url: 'https://admin:Password123@devniz.atlassian.net/rest/api/latest/issue/' + constructedStoryTicketNumber,
            method: 'PUT',
            json:
            {
              "fields":
              {
                "customfield_10129": parseInt(storyPoint)
              }
            }
          },
          (error, response, body) => {
            if (response.statusCode < 200 || response.statusCode > 299) {
              reject(response.statusCode);
            } else {
              resolve(response.statusCode);
            }
          });
        });

      }

      /**
      * This module use the JIRA REST API,
      * it is dedicated to call only endpoint for /issues requests.
      **/
      var storyModule = {
        getStoryStatus: getStoryStatus,
        deleteStory: deleteStory,
        assignUserToStory: assignUserToStory,
        transitStory: transitStory,
        createStory: createStory,
        getStoryPoint: getStoryPoint,
        estimateStory: estimateStory
      }

      module.exports = storyModule;
