var expect     = require('chai').expect;
var storyApi   = require('../modules/atlassian-jira-rest-api-for-issues');

describe('Story API Tests', function() {
  beforeEach((done) => {
    done();
  });

  afterEach((done) => {
    done();
  })

  it('It should get the story status given a valid story ticket number', function(done) {
    var url = 'https://devniz.atlassian.net';

    storyApi.getStoryStatus('20').then((res) => {
      expect(res).to.be.a('string');
      expect(res).to.be.eql('Done');

      done();
    })
  });

  it('It should delete a story with success', function(done) {
    storyApi.deleteStory('17').then((res) => {
      expect(res).to.be.eql(204);
      done();
    })
  });

  it ('It should transit a story', function(done) {
    storyApi.transitStory('20', 'in progress').then((res) => {
      expect(res).to.be.eql(204);

      done();
    });
  });

  it ('It should assign a story to a username', function(done) {
    storyApi.assignUserToStory('17', 'mike').then((res) => {
      expect(res).to.be.eql(204);
    });
  });

  it('It should get the current story point', function(done) {
    storyApi.getStoryPoint('20').then((res) => {
      expect(res).to.be.eql(204);

      done();
    });
  });

  it('It should estimate a story', function(done) {
    storyApi.estimateStory('20', '3').then((res) => {
      expect(res).to.be.eql(204);

      done();
    });
  });

})
